package foley.demo.junit5;

import org.junit.jupiter.api.*;

import static foley.demo.junit5.Location.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("(3 Exception) Customer")
class ExceptionTest {


    @DisplayName("can't have blank name")
    @Test
    void cantHaveBlankName() {
        assertThrows(
                IllegalArgumentException.class, 
                () -> Customer.of("", UK));
    }

    @DisplayName("when created with blank name provides a meaningful message")
    @Test
    void blankNameHasMeaningfulMessage() {
        Throwable e = assertThrows(
                IllegalArgumentException.class,
                () -> Customer.of(" ", UK));
        assertEquals(e.getMessage(), "Customer Name cannot be blank");
    }
    
    @DisplayName("can be a UK Customer")
    @Test
    void canBeUkCustomer() {
        Customer customer = Customer.of("MicroFish industries", UK); 
       
        assertThat(customer, is(instanceOf(UkCustomer.class)));
        assertThat(customer, hasToString("MicroFish industries"));
    }

    @DisplayName("can be a ROW Customer")
    @Test
    void canBeRowCustomer() {
        Customer customer = Customer.of("Läsbar Programvara", SWEDEN);

        assertThat(customer, is(instanceOf(RowCustomer.class)));
        assertThat(customer, hasToString("Läsbar Programvara (Sweden)"));
    }
}
                                                                   