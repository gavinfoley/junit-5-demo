package foley.demo.junit5;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.*;
import org.junit.jupiter.params.provider.*;

import java.util.stream.*;

import static foley.demo.junit5.Location.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("(4 Parameterised) Customer")
class ParameterisedTests {

    @DisplayName("can't have blank name")
    @ParameterizedTest
    @ValueSource(strings = {"", " "})
    void cantHaveBlankName(String candidate) {
        Throwable e = assertThrows(
                IllegalArgumentException.class,
                () -> Customer.of(candidate, UK));
        assertEquals(e.getMessage(), "Customer Name cannot be blank");
    }

    @DisplayName("can't have an invalid name (method source)")
    @ParameterizedTest
    @MethodSource("invalidNames")
    void cantHaveBlankNameImproved(String candidate) {
        Throwable e = assertThrows(
                IllegalArgumentException.class,
                () -> Customer.of(candidate, UK));
        assertEquals(e.getMessage(), "Customer Name cannot be blank");
    }
    
    static Stream<String> invalidNames() {
       return Stream.of("", " ", null); 
    }
    

    @DisplayName("can be a ROW Customer")
    @Test
    void canBeRowCustomer() {
        Customer customer = Customer.of("Läsbar Programvara", SWEDEN);

        assertThat(customer, is(instanceOf(RowCustomer.class)));
        assertThat(customer, hasToString("Läsbar Programvara (Sweden)"));
    }
}
                                                                   