package foley.demo.junit5;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.*;
import org.junit.jupiter.params.provider.*;

import java.util.stream.*;

import static foley.demo.junit5.CustomerLeadTime.*;
import static foley.demo.junit5.CustomerShipping.*;
import static foley.demo.junit5.Location.*;
import static foley.demo.junit5.Shipping.*;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("(5 Nested) Customer")
class NestedTests {

    @DisplayName("can't have an invalid name")
    @ParameterizedTest
    @MethodSource("invalidNames")   //can't be in a nested class
    void cantHaveBlankNameImproved(String candidate) {
        Throwable e = assertThrows(
                IllegalArgumentException.class,
                () -> Customer.of(candidate, UK));
        assertEquals(e.getMessage(), "Customer Name cannot be blank");
    }

    static Stream<String> invalidNames() {
        return Stream.of("", " ", null);
    }

    @DisplayName("in the UK")
    @Nested
    class Uk {
        Customer customer = Customer.of("MicroFish industries", UK);
        
        @DisplayName("receives deliveries by Royal Mail")
        @Test
        void shippingIsByRoyalMail() {
            Shipping shipping = customer.map(TO_SHIPPING);
            
            assertEquals(ROYAL_MAIL, shipping);
        }

        @DisplayName("lead time is 2 days")
        @Test
        void leadTimeIsTwoDays() {
            int leadTimeInDays = customer.map(TO_LEAD_TIME_IN_DAYS);

            assertEquals(2, leadTimeInDays);
        }
    }

    @DisplayName("in the ROW")
    @Nested
    class Row {
        Customer customer = Customer.of("Läsbar Programvara", SWEDEN);
        
        @DisplayName("receives deliveries by Air Mail")
        @Test
        void shippingIsByRoyalMail() {
            Shipping shipping = customer.map(TO_SHIPPING);

            assertEquals(AIR_MAIL, shipping);
        }

        @DisplayName("lead time is 7 days")
        @Test
        void leadTimeIsTwoDays() {
            int leadTimeInDays = customer.map(TO_LEAD_TIME_IN_DAYS);

            assertEquals(7, leadTimeInDays);
        }
    }
}
                                                                   