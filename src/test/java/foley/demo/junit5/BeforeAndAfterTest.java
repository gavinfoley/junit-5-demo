package foley.demo.junit5;

import org.junit.jupiter.api.*;

import static foley.demo.junit5.Location.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

@DisplayName("(2 Before and After) Customer")
class BeforeAndAfterTest {
 
    @BeforeAll
    static void beforeAll() {
        System.out.println("before all");
    }
    
    @BeforeEach
    void beforeEach() {
        System.out.println("before each");
    }

    @AfterEach
    void afterEach() {
        System.out.println("after each");
    }

    @AfterAll 
    static void afterAll() {
        System.out.println("after all");
    }

    @DisplayName("can be a UK Customer")
    @Test
    void canBeUkCustomer() {
       Customer customer = Customer.of("MicroFish industries", UK); 
       
       assertThat(customer, is(instanceOf(UkCustomer.class)));
    }

    @DisplayName("can be a ROW Customer")
    @Test
    void canBeRowCustomer() {
        Customer customer = Customer.of("Läsbar Programvara", SWEDEN);

        assertThat(customer, is(instanceOf(RowCustomer.class)));
    }
}
