package foley.demo.junit5;

import org.junit.jupiter.api.*;

import static foley.demo.junit5.Location.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

@DisplayName("(1 DisplayName) Customer")
class DisplayNameTest {
 
    @DisplayName("can be a UK Customer")
    @Test
    void canBeUkCustomer() {
       Customer customer = Customer.of("MicroFish industries", UK); 
       
       assertThat(customer, is(instanceOf(UkCustomer.class)));
       assertThat(customer, hasToString("MicroFish industries"));       
    }

    @DisplayName("can be a ROW Customer")
    @Test
    void canBeRowCustomer() {
        Customer customer = Customer.of("Läsbar Programvara", SWEDEN);

        assertThat(customer, is(instanceOf(RowCustomer.class)));
        assertThat(customer, hasToString("Läsbar Programvara (Sweden)"));
    }
}
