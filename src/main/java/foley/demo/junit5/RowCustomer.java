package foley.demo.junit5;

import static java.lang.String.format;

class RowCustomer implements Customer {
    private final String name;
    private final Location location;

    RowCustomer(String name, Location location) {
        this.name = name;
        this.location = location;
    }

    @Override
    public String toString() {
        return format("%s (%s)", name, location);
    }

    @Override
    public <T> T map(CustomerVisitor<T> visitor) {
        return visitor.map(this);
    }
}
