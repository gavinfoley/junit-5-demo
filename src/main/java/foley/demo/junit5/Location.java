package foley.demo.junit5;

enum Location {
    SWEDEN("Sweden"), UK("United Kingdom");

    private final String name;

    Location(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
