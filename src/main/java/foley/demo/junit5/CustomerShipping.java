package foley.demo.junit5;

import static foley.demo.junit5.Shipping.AIR_MAIL;
import static foley.demo.junit5.Shipping.ROYAL_MAIL;

class CustomerShipping implements CustomerVisitor<Shipping> {
    static final CustomerShipping TO_SHIPPING = new CustomerShipping();

    @Override
    public Shipping map(UkCustomer customer) {
        return ROYAL_MAIL;
    }

    @Override
    public Shipping map(RowCustomer customer) {
        return AIR_MAIL;
    }
}
