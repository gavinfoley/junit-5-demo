package foley.demo.junit5;

interface CustomerVisitor<T> {
    
    T map(UkCustomer customer);
    T map(RowCustomer customer);
}
