package foley.demo.junit5;

class UkCustomer implements Customer {
    private final String name;

    UkCustomer(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public <T> T map(CustomerVisitor<T> visitor) {
        return visitor.map(this);
    }
}
