package foley.demo.junit5;

class CustomerLeadTime implements CustomerVisitor<Integer> {
    static final CustomerVisitor<Integer> TO_LEAD_TIME_IN_DAYS = new CustomerLeadTime();

    @Override
    public Integer map(UkCustomer customer) {
        return 2;
    }

    @Override
    public Integer map(RowCustomer customer) {
        return 7;
    }
}
