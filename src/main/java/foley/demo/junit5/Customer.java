package foley.demo.junit5;

import java.util.*;

import static foley.demo.junit5.Location.UK;

interface Customer {

    <T> T map(CustomerVisitor<T> visitor);

    static Customer of(String name, Location location) {
        if (location == UK)
            return new UkCustomer(valid(name));
        else
            return new RowCustomer(valid(name), location);
    }

    static String valid(String name) {
        return Optional.ofNullable(name)
                    .map(String::trim)
                    .filter(n -> !n.isEmpty())
                    .orElseThrow(() -> new IllegalArgumentException("Customer Name cannot be blank"));
    }
}
